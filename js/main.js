$(document).ready(function () {
    "use strict";
    // creat menu sidebar
    $(".menu-bar-lv-1").each(function () {
        $(this).find(".span-lv-1").click(function () {
            $(this).toggleClass('rotate-menu');
            $(this).parent().find(".menu-bar-lv-2").toggle(500);
        });
    });
    $(".menu-bar-lv-2").each(function () {
        $(this).find(".span-lv-2").click(function () {
            $(this).toggleClass('rotate-menu');
            $(this).parent().find(".menu-bar-lv-3").toggle(500);
        });
    });
    $(".shadow-open-menu").click(function () {
        $('.menu-bar-mobile').fadeOut();
        $(".shadow-open-menu").fadeOut();
        $(".menu-btn-show").toggleClass("active");
    });
    $(".menu-btn-show").click(function () {
        $(this).toggleClass("active");
        $('.menu-bar-mobile').fadeToggle(500);
        $(".shadow-open-menu").fadeToggle(500);
    });
    // end
});



$("#icon-search-mobile").click(function () {
	$('.search-box').toggleClass('open');
});
$(".main-menu ul li i").click(function () {
	$(this).next("ul").toggleClass('open');
});

$(document).ready(function () {
    $('#banner_home').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots:true,
        items:1,
    });
    
});